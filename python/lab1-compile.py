#!/usr/bin/env python3
from subprocess import run

C_FLAGS = "-O3 -Wall -Werror"

COMPILERS = {
    'gcc': {
        'cc': 'gcc-8',
        'seq': "{flags} lab1.c -o lab1-gcc-seq -lm",
        'par': "{flags} -fdump-tree-parloops-all -floop-parallelize-all -ftree-parallelize-loops={k} "
               "lab1.c -o lab1-gcc-par-{k} -lm"
    },
    'solarcc': {
        'cc': 'solarcc',
        'seq': '-I/usr/include/x86_64-linux-gnu/ lab1.c -o lab1-solarcc-seq -lm',
        'par': '-I/usr/include/x86_64-linux-gnu/ -xautopar -xloopinfo lab1.c -o lab1-solarcc-par-{k} -lm'
    },
    'icc': {
        'cc': 'icc',
        'seq': '-o lab1-icc-seq lab1.c',
        'par': '-parallel -qopt-report-phase=par -par-threshold={k} -o lab1-icc-par-{k} lab1.c'
    }
}


def compile(c: str, cores=(2, 3, 4, 5, 6, 8)):
    if c not in COMPILERS:
        return
    d = COMPILERS[c]
    cc = [d['cc']]
    print(cc)
    flag_seq = d['seq'].format(flags=C_FLAGS)
    run(cc + flag_seq.split())
    for k in cores:
        flag_par = d['par'].format(flags=C_FLAGS, k=k)
        print(cc + flag_par.split())
        run(cc + flag_par.split())
    pass


if __name__ == '__main__':
    compile('gcc')
    compile('solarcc', cores=('n',))
    compile('icc')
