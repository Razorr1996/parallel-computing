#!/usr/bin/env python3
import subprocess

TARGET1 = 10
TARGET2 = 2000
exe = 'lab1-seq'

def find_n(n):
    start = 0
    end = 2
    time = 0
    while time < n:
        print('----')
        print(start, end, time)
        s = subprocess.check_output('./lab1-gcc-seq ' + str(end), shell=True).decode("utf-8")
        s = s.strip().split('\n')
        # _ = int(s[0])
        time = int(s[1])
        start = end
        end *= 2
    while abs(end - start) > 1:
        mid = (end + start) // 2
        s = subprocess.check_output('./lab1-gcc-seq ' + str(end), shell=True).decode("utf-8")
        s = s.strip().split('\n')
        # N = int(s[0])
        time = int(s[1])
        print('----')
        print(start, end, time)
        if time < n:
            start = mid
        else:
            end = mid
    return end


if __name__ == '__main__':
    print('N1 = ', find_n(TARGET1))
    print('N2 = ', find_n(TARGET2))
    # for mid in range(N1, N2, d):
    #     p = os.popen('./1lab-seq ' + str(mid), "r")
    #     N = int(p.readline())
    #     time = int(p.readline())
    #     p.close()
    #     print(N, time)
    # pass
