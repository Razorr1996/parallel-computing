#!/usr/bin/env python3
import csv
import subprocess
import sys
from pprint import pprint

exe_seq = './lab1-{cc}-seq {n}'
exe_par = './lab1-{cc}-par-{k} {n}'

start_csv = ['name', 'N1', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'N2']


def get_time(exe):
    s = subprocess.check_output(exe, shell=True).decode("utf-8")
    s = s.strip().split('\n')
    return int(s[1])


def work(n1, n2, cc, cores=(2, 3, 4, 5, 6, 8)):
    delta = (n2 - n1) / 10
    csv_data = [start_csv]
    csv_seq = ['lab1-{cc}-seq'.format(cc=cc)]
    ns = [round(N1 + i * delta) for i in range(11)]
    for n in ns:
        t = get_time(exe_seq.format(n=n, cc=cc))
        csv_seq.append(str(t))
    csv_data.append(csv_seq)
    for k in cores:
        csv_par = ['lab1-{cc}-par-{k}'.format(k=k, cc=cc)]
        for n in ns:
            t = get_time(exe_par.format(n=n, k=k, cc=cc))
            csv_par.append(t)
        csv_data.append(csv_par)
    pprint(csv_data)
    with open('lab1_1_{cc}_result.csv'.format(cc=cc), 'w') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerows(csv_data)


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('Usage: lab1-task8.py N1 N2')
        exit(1)
    N1, N2 = map(int, sys.argv[1:3])
    work(N1, N2, 'gcc')
    work(N1, N2, 'solarcc', ('n',))
    work(N1, N2, 'icc')
    pass
