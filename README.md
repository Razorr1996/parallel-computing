 ## How to start labs?
```shell script
сd <lab dir>
cmake . && make
./lab<number>-seq
```

## Fix using AMD Framewave on macOS ##
```shell script
export LIB_DIR=/usr/local/lib/64
sudo mkdir -p "$LIB_DIR"
sudo ln -f -s $(pwd)/library/darwin/libfwBase.1.dylib "$LIB_DIR"
sudo ln -f -s $(pwd)/library/darwin/libfwSignal.1.dylib "$LIB_DIR"
```
